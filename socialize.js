/*!
 *
 * SOCIALIZE v0.3.2
 *
 * @author  Magno Biét
 * @email   magno.biet@gmail.com
 *
 * @licence http://magno.mit-license.org/2014
 *
 */

var SOCIALIZE = (function(window, document, undefined) {

	'use strict';

	try {

		var get = (function() {

			var container = {
					twitter: '.twitter-share-button, .twitter-follow-button, .twitter-tweet, .twitter-timeline',
					facebook: '.fb-like, .fb-share-button, .fb-send, .fb-post, .fb-follow, .fb-comments, .fb-activity, .fb-recommendations-bar, .fb-like-box, .fb-facepile',
					pinterest: 'a[data-pin-do]',
					linkedin: 'script[type="IN/Share"], script[type="IN/FollowCompany"], script[type="IN/MemberProfile"], script[type="IN/CompanyInsider"] script[type="IN/CompanyProfile"] script[type="IN/Apply"], script[type="IN/RecommendProduct"], script[type="IN/JYMBII"]',
					plusone: '.g-plusone',
					disqus: '#disqus_thread'
				};

			function getScript(id, src) {

				var js,
					d = document,
					s = 'script',
					sjs = d.getElementsByTagName(s)[0];

				if (!d.getElementById(id)) {

					js = d.createElement(s);
					js.id = id;
					js.async = true;
					js.src = src;
					sjs.parentNode.insertBefore(js, sjs);
				}

			}

			/* https://dev.twitter.com */
			function twitter() {

				if (document.querySelectorAll(container.twitter).length > 0) {

					getScript('twitter-wjs', 'https://platform.twitter.com/widgets.js');

				}

			}

			/* https://developers.facebook.com/docs/plugins */
			function facebook() {

				if (document.querySelectorAll(container.facebook).length > 0) {

					getScript('facebook-jssdk', '//connect.facebook.net/en_US/all.js#xfbml=1');

				}

			}

			/* https://developers.pinterest.com */
			function pinterest() {

				if (document.querySelectorAll(container.pinterest).length > 0) {

					getScript('pinterest-sdk', '//assets.pinterest.com/js/pinit.js');

				}

			}

			/* http://developer.linkedin.com/plugins */
			function linkedin() {

				if (document.querySelectorAll(container.linkedin).length > 0) {

					getScript('linkedin-sdk', '//platform.linkedin.com/in.js');

				}

			}

			/* https://developers.google.com/+/web/+1button */
			function plusOne() {

				if (document.querySelectorAll(container.plusone).length > 0) {

					getScript('googleplus-sdk', 'https://apis.google.com/js/plusone.js?onload=onLoadCallback');

				}

			}

			/* http://disqus.com/api/docs/ */
			function disqus(disqus_shortname) {

				if (document.querySelectorAll(container.disqus).length > 0) {

					getScript('disqus-sdk', '//' + disqus_shortname + '.disqus.com/count.js');

				}

			}

			function all() {

				get.twitter();
				get.facebook();
				get.pinterest();
				get.linkedin();
				get.plusOne();

			}

			return {
				twitter: twitter,
				facebook: facebook,
				pinterest: pinterest,
				linkedin: linkedin,
				plusOne: plusOne,
				disqus: disqus,
				all: all
			};

		}());

		var init = (function() {

			return get.all;

		}());

		return {
			get: get,
			init: init
		};

	} catch (e) {

		throw e.message;
	}

}(window, document));
